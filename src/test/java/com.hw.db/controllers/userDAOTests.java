package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;

public class userDAOTests {

    private User testUser;
    private JdbcTemplate mockJdbc;
    private UserDAO userDAO;

    @BeforeEach
    void setUp() {
        mockJdbc = mock(JdbcTemplate.class);
        userDAO = new UserDAO(mockJdbc);
    }


    @Test
    void changeTest1() {
        testUser = new User(
                "Berdina",
                "email.com",
                null,
                null
        );

        try (MockedStatic<ThreadDAO> ignored = Mockito.mockStatic(ThreadDAO.class)) {
            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;",
                    testUser.getEmail(), testUser.getNickname());

        }
    }

    @Test
    void changeTest2() {
        testUser = new User(
                "Berdina",
                null,
                "Oydinoy",
                null
        );

        try (MockedStatic<ThreadDAO> ignored = Mockito.mockStatic(ThreadDAO.class)) {
            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;",
                    testUser.getFullname(), testUser.getNickname());

        }
    }

    @Test
    void changeTest3() {
        testUser = new User(
                "Berdina",
                null,
                null,
                "Am writing test cases"
        );

        try (MockedStatic<ThreadDAO> ignored = Mockito.mockStatic(ThreadDAO.class)) {
            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;",
                    testUser.getAbout(), testUser.getNickname());

        }
    }

    @Test
    void changeTest4() {
        testUser = new User(
                "Berdina",
                null,
                null,
                null
        );

        try (MockedStatic<ThreadDAO> ignored = Mockito.mockStatic(ThreadDAO.class)) {
            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Mockito.anyCollection());

        }
    }
}