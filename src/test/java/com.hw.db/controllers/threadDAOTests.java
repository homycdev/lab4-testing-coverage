package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class threadDAOTests {

    private List<Object> emptyArray;
    private JdbcTemplate mockJdbc;
    private ThreadDAO threadDAO;


    @BeforeEach
    @DisplayName("stubs for testing")
    void setUp() {
        emptyArray = Collections.emptyList();
        mockJdbc = mock(JdbcTemplate.class);
        threadDAO = new ThreadDAO(mockJdbc);

    }

    @Test
    void treeSort1() {
        assertEquals(emptyArray, ThreadDAO.treeSort(1, 1, 1, true));
    }

    @Test
    void treeSort2() {
        assertEquals(emptyArray, ThreadDAO.treeSort(1, 1, 1, false));
    }

}