package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;

import static org.mockito.Mockito.mock;

public class postDAOTests {

    private Post testPost;
    private Post newTestPost;
    private JdbcTemplate mockJdbc;
    private PostDAO postDAO;
    private PostDAO.PostMapper POST_MAPPER;

    @BeforeEach
    void setUp() {
        mockJdbc = mock(JdbcTemplate.class);
        postDAO = new PostDAO(mockJdbc);
        POST_MAPPER = new PostDAO.PostMapper();

    }

    @Test
    void setPostTest1() {
        testPost = new Post(
                "sampleAuthor",
                new Timestamp(0),
                "forum",
                "message",
                1,
                12,
                true);

        newTestPost = new Post(
                "sampleAuthor2",
                new Timestamp(1),
                "forum",
                "new message",
                1,
                12,
                true);


        Mockito.when(mockJdbc.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(newTestPost);
        PostDAO.setPost(1, testPost);
        Mockito.verify(mockJdbc).update("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;", testPost.getAuthor(), testPost.getMessage(), testPost.getCreated(), 1);
    }

    @Test
    void setPostTest2() {
        testPost = new Post(
                "sampleAuthor",
                null,
                "forum",
                "message",
                1,
                12,
                true);

        newTestPost = new Post(
                "sampleAuthor2",
                null,
                "forum",
                "new message",
                1,
                12,
                true);

        Mockito.when(mockJdbc.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(newTestPost);
        PostDAO.setPost(1, testPost);
        Mockito.verify(mockJdbc).update("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;", testPost.getAuthor(), testPost.getMessage(), 1);
    }

    @Test
    void setPostTest3() {
        testPost = new Post(
                "sampleAuthor",
                new Timestamp(0),
                "forum",
                null,
                1,
                12,
                true);

        newTestPost = new Post(
                "sampleAuthor2",
                new Timestamp(1),
                "forum",
                null,
                1,
                12,
                true);


        Mockito.when(
                mockJdbc.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1)))
                .thenReturn(newTestPost);
        PostDAO.setPost(1, testPost);
        Mockito.verify(mockJdbc).update("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;", testPost.getAuthor(), testPost.getCreated(), 1);
    }

    @Test
    void setPostTest4() {
        testPost = new Post(
                null,
                new Timestamp(0),
                "forum",
                "message",
                1,
                12,
                true);

        newTestPost = new Post(
                null,
                new Timestamp(1),
                "forum",
                "new message",
                1,
                12,
                true);


        Mockito.when(mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1)))
                .thenReturn(newTestPost);
        PostDAO.setPost(1, testPost);
        Mockito.verify(mockJdbc).update("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;", testPost.getMessage(), testPost.getCreated(), 1);
    }


    @Test
    void setPostTest5() {
        testPost = new Post(
                "sampleAuthor",
                new Timestamp(0),
                "forum",
                "message",
                1,
                12,
                true);

        newTestPost = new Post(
                "sampleAuthor",
                new Timestamp(0),
                "forum",
                "new message",
                1,
                12,
                true);


        Mockito.when(
                mockJdbc.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1)))
                .thenReturn(newTestPost);
        PostDAO.setPost(1, testPost);
        Mockito.verify(mockJdbc).update("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;", testPost.getMessage(), 1);
    }


    @Test
    void setPostTest6() {
        testPost = new Post(
                "sampleAuthor",
                new Timestamp(0),
                "forum",
                "message",
                1,
                12,
                true);

        newTestPost = new Post(
                "sampleAuthor",
                new Timestamp(1),
                "forum",
                "message",
                1,
                12,
                true);


        Mockito.when(
                mockJdbc.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.eq(1)))
                .thenReturn(newTestPost);
        PostDAO.setPost(1, testPost);
        Mockito.verify(mockJdbc).update("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;", testPost.getCreated(), 1);
    }

    @Test
    void setPostTest7() {
        testPost = new Post(
                "sampleAuthor",
                new Timestamp(0),
                "forum",
                "message",
                1,
                12,
                true);

        newTestPost = new Post(
                "sampleAuthor2",
                new Timestamp(0),
                "forum",
                "message",
                1,
                12,
                true);


        Mockito.when(
                mockJdbc.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.eq(1)))
                .thenReturn(newTestPost);
        PostDAO.setPost(1, testPost);
        Mockito.verify(mockJdbc).update("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;", testPost.getAuthor(), 1);
    }

    @Test
    void setPostTest8() {
        testPost = new Post(
                "sampleAuthor",
                new Timestamp(0),
                "forum",
                "message",
                1,
                12,
                true);

        newTestPost = new Post(
                "sampleAuthor",
                new Timestamp(0),
                "forum",
                "message",
                1,
                12,
                true);


        Mockito.when(
                mockJdbc.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.eq(1)))
                .thenReturn(newTestPost);
        PostDAO.setPost(1, testPost);
        Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Mockito.anyCollection());
    }
}