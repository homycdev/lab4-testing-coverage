package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class forumDAOTests {
    private JdbcTemplate mockJdbc;
    private ForumDAO forumDAO;
    private ThreadDAO.ThreadMapper threadMapper;
    private UserDAO.UserMapper userMapper;

    @BeforeEach
    void setUp() {
        mockJdbc = mock(JdbcTemplate.class);
        forumDAO = new ForumDAO(mockJdbc);
        threadMapper = new ThreadDAO.ThreadMapper();
        userMapper = new UserDAO.UserMapper();
    }

    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        ForumDAO.ThreadList("slug", null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"),
                Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #2")
    void ThreadListTest2() {
        ForumDAO.ThreadList("slug", null, "12.02.2019", false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)" +
                        "::TIMESTAMP WITH TIME ZONE ORDER BY created;"), Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #3")
    void ThreadListTest3() {
        ForumDAO.ThreadList("slug", null, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)" +
                        "::TIMESTAMP WITH TIME ZONE ORDER BY created desc;"), Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #4")
    void ThreadListTest4() {
        ForumDAO.ThreadList("slug", 10, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)" +
                        "::TIMESTAMP WITH TIME ZONE ORDER BY created desc LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #5")
    void ThreadListTest5() {
        ForumDAO.ThreadList("slug", 10, "12.02.2019", false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)" +
                        "::TIMESTAMP WITH TIME ZONE ORDER BY created LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #6")
    void ThreadListTest6() {
        ForumDAO.ThreadList("slug", 10, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created desc " +
                "LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    void testUserList1() {
        ForumDAO.UserList("slug", null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext ORDER BY nickname;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList2() {
        ForumDAO.UserList("slug", null, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                        "::citext ORDER BY nickname desc;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList3() {
        ForumDAO.UserList("slug", null, "12.02.2019", false);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                        "::citext AND  nickname > (?)::citext ORDER BY nickname;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList4() {
        ForumDAO.UserList("slug", null, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                        "::citext AND  nickname < (?)::citext ORDER BY nickname desc;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList5() {
        ForumDAO.UserList("slug", 10, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                        "::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList6() {
        ForumDAO.UserList("slug", 10, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                        "::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList7() {
        ForumDAO.UserList("slug", 10, "12.09.2019", null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                        "::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList8() {
        ForumDAO.UserList("slug", 10, "12.09.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                        "::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }
}
